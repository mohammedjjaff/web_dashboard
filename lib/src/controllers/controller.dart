import 'package:get/get.dart';

class Controller extends GetxController {


  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
  }


  /// open and close the side bar
  RxBool isOpen = true.obs;
  RxBool showIt = true.obs;
  RxBool isLoad = false.obs;
  changeStatusOfDrawer(){
    if(isLoad.value == false){
      isLoad.value = true;
      if(isOpen.value){
        isOpen.value = false;
        showIt.value = false;
        isLoad.value = false;
      }else{
        isOpen.value = true;
        Future.delayed(const Duration(milliseconds: 250),(){
          showIt.value = true;
          isLoad.value = false;
        });
      }
    }
  }


  /// hover of elements
  RxInt hoverIndex = 100000.obs;
  changeHover(int index){
    hoverIndex.value = index;
  }


  /// page selected
  RxInt pageSelected = 0.obs;
  changePage(int index){
    pageSelected.value = index;
  }
}