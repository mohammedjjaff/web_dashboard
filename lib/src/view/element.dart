import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../controllers/controller.dart';

class ButtonElement extends StatelessWidget {
  final int index;
  final IconData icon;
  final String title;
  final Color? indicatorColor;
  final TextStyle? titleStyle;
  final Widget? leading;
  const ButtonElement({super.key,required this.index,required this.icon,required this.title,this.indicatorColor,this.titleStyle,this.leading});

  @override
  Widget build(BuildContext context) {
    final Controller controller = Get.find();
    return Padding(
      padding: const EdgeInsets.only(bottom: 5),
      child: InkWell(
        onHover: (value){
          controller.changeHover(value ? index : 10000);
        },
        onTap: (){
          controller.changePage(index);
        },
        child: Obx(() {
          if(controller.showIt.value){
            return AnimatedContainer(
              duration: const Duration(milliseconds: 250),
              margin: const EdgeInsets.only(left: 30),
              height: controller.hoverIndex.value == index || controller.pageSelected.value == index ? 45 : 35,
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(15),
                  bottomLeft: Radius.circular(15)
                ),
                color: controller.hoverIndex.value == index || controller.pageSelected.value == index ? (indicatorColor ?? Colors.pinkAccent) : Colors.transparent
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      AnimatedContainer(
                        duration: const Duration(milliseconds: 250),
                        height: 30,
                        width: controller.hoverIndex.value == index || controller.pageSelected.value == index ? 10 : 0,
                        decoration: BoxDecoration(
                            borderRadius: const BorderRadius.only(
                                topLeft: Radius.circular(10),
                                bottomLeft: Radius.circular(10)
                            ),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                  offset: const Offset(-1,-1),
                                  color: controller.hoverIndex.value == index || controller.pageSelected.value == index ? Colors.white : Colors.transparent,
                                  spreadRadius: 1,
                                  blurRadius: 10
                              )
                            ]
                        ),
                      ),
                      AnimatedContainer(
                        duration: const Duration(milliseconds: 250),
                        height: 40,
                        width: controller.hoverIndex.value == index || controller.pageSelected.value == index ? 20 : 30,
                      ),
                      Icon(icon,size: 20,color: (controller.hoverIndex.value == index || controller.pageSelected.value == index ? Colors.white : Colors.black54),),
                      const SizedBox(width: 5,),
                      Text(title,style: titleStyle ?? TextStyle(
                        color: (controller.hoverIndex.value == index || controller.pageSelected.value == index ? Colors.white : Colors.black54),
                        fontSize: 14,
                      ),)
                    ],
                  ),
                  leading ?? const SizedBox()
                ],
              ),
            );
          }else{
            return Icon(icon,size: 20,color: (controller.hoverIndex.value == index || controller.pageSelected.value == index ? Colors.black : Colors.black.withOpacity(0.3)),);
          }
        }),
      ),
    );
  }
}
