import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:web_dashboard/src/controllers/controller.dart';
import 'package:web_dashboard/src/models/headerModel.dart';

class Header extends StatelessWidget {
  final HeaderModel header;
  const Header({super.key,required this.header});

  @override
  Widget build(BuildContext context) {
    final Controller controller = Get.find();
    return AnimatedPadding(
      duration: const Duration(milliseconds: 250),
      padding: EdgeInsets.only(right: controller.isOpen.value ? 30 : 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  GestureDetector(
                    onTap: (){
                      controller.changeStatusOfDrawer();
                    },
                    child: Container(
                      width: 50,
                      height: 50,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(100),
                          gradient: LinearGradient(
                              colors: [Colors.white.withOpacity(0.5),Colors.transparent],
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter
                          )
                      ),
                      child: Center(
                        child: Container(
                          width: 48,
                          height: 48,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100),
                            border: Border.all(color: Colors.white,width: 3),
                          ),
                          child: Center(
                            child: header.imagePath == null ? const FlutterLogo() : Image.asset(header.imagePath!,fit: BoxFit.contain,),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Obx(() {
                    if(controller.showIt.value){
                      return const SizedBox(width: 15,);
                    }else{
                      return const SizedBox();
                    }
                  }),
                  Obx(() {
                    if(controller.showIt.value){
                      return Text(header.title,style: header.headerTextStyle ?? const TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 14
                      ));
                    }else{
                      return const SizedBox();
                    }
                  }),
                ],
              ),
              Obx(() {
                if(controller.showIt.value){
                  return GestureDetector(
                    onTap: (){controller.changeStatusOfDrawer();},
                    child: Container(
                      height: 40,
                      width: 15,
                      decoration: BoxDecoration(
                        borderRadius: const BorderRadius.only(
                            topRight: Radius.circular(10),
                            bottomRight: Radius.circular(10)
                        ),
                        color: const Color(0xffd8d8d8).withOpacity(0.7),
                      ),
                      child: const Center(
                        child: Icon(Icons.arrow_right,color: Colors.black,size: 15,),
                      ),
                    ),
                  );
                }else{
                  return const SizedBox();
                }
              }),
            ],
          ),
          const SizedBox(height: 10,),
          AnimatedContainer(
            duration: const Duration(milliseconds: 250),
            width: Get.width,
            height: 1,
            color: Colors.black.withOpacity(0.1),
            margin: EdgeInsets.only(left: controller.isOpen.value ? 30 : 20),
          ),
        ],
      ),
    );
  }
}
