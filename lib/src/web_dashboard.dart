import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:web_dashboard/src/controllers/controller.dart';
import 'package:web_dashboard/src/models/headerModel.dart';
import 'package:web_dashboard/src/view/element.dart';
import 'package:web_dashboard/src/view/header.dart';

class WebDashboard extends StatelessWidget {
  final List<ButtonElement> elements;
  final Widget body;
  final HeaderModel headerContent;
  final dynamic onTap;
  const WebDashboard({super.key,required this.elements,required this.body,required this.headerContent,this.onTap});

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      home: GetBuilder<Controller>(
        init: Controller(),
        builder: (Controller controller) {
          return Directionality(
            textDirection: TextDirection.rtl,
            child: Scaffold(
              body: Row(
                children: [
                  Obx(() => AnimatedContainer(
                    duration: const Duration(milliseconds: 250),
                    height: MediaQuery.of(context).size.height,
                    width: controller.isOpen.value ? (MediaQuery.of(context).size.width * 0.17) : (MediaQuery.of(context).size.width * 0.065),
                    color: Colors.white,
                    padding: const EdgeInsets.only(top: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Header(header: headerContent,),
                        const SizedBox(height: 20,),
                        Expanded(
                          child: ListView.builder(
                            itemCount: elements.length,
                            itemBuilder: (context,index){
                              return GestureDetector(
                                onTap: onTap!,
                                child: ButtonElement(index: index, icon: elements[index].icon, title: elements[index].title,indicatorColor: elements[index].indicatorColor,titleStyle: elements[index].titleStyle,leading: elements[index].leading,),
                              );
                            },
                          ),
                        )
                      ],
                    ),
                  )),
                  Expanded(
                    child: Container(
                      height: MediaQuery.of(context).size.height,
                      color: const Color(0xffd8d8d8).withOpacity(0.2),
                      child: body,
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}

