
import 'package:flutter/material.dart';

class HeaderModel {
  String? imagePath;
  TextStyle? headerTextStyle;
  late String title;
  HeaderModel({this.imagePath,this.headerTextStyle,required this.title});
}